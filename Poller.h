#ifndef POLLER_H
#define POLLER_H

#include <sys/select.h>
#include <vector>

using std::vector;
using std::pair;

class Poller {
    public:
        Poller();
        void add_fd(int fd);
        void set_writable(int fd, bool write);
        void remove_fd(int fd);
        vector<int> poll() const;
    private:
        fd_set read_fds;
        fd_set write_fds;
        int max_fd;

};

#endif
