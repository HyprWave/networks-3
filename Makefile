CFLAGS += --std=c++0x -Wall -g
CC = g++

all: file_server file_client

file_server: 
	$(CC) $(CFLAGS) ServerProgram.cpp Server.cpp Socket.cpp Connection.cpp Poller.cpp -o file_server

file_client: 
	$(CC) $(CFLAGS) ClientProgram.cpp Socket.cpp Connection.cpp Poller.cpp -o file_client

clean:
	rm file_server file_client
