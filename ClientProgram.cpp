#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <dirent.h>
#include <cstring>
#include "Socket.h"
#include "Connection.h"
#include "Poller.h"

#define BUFFERSIZE 2048
#define SERVER_PORT "2511"
#define CLIENT_PORT 2512

using namespace std;

enum ClientState {
    cWaitingForUser,
    cWelcome,
    cSendFileList,
    cSentFileList,
    cGetFileList,
    cGetFileServerSide,
    cGetFile,
    cSending,
    cClosing
};

static string dir_path;
static enum ClientState c_state;
static enum ClientState c_nstate;
static Socket server;
static string s_tmp;
static string get_req;
static string filename;
static Socket listening;
static ConnectionPtr file_sender;
static map<int, ConnectionPtr> connections;
static Poller poller;

void handle_user_input() {
    std::string cmd, arg;
    cin >> cmd;
    if (cmd == "DIR") {
        s_tmp = "D";
        c_state = cSending;
        c_nstate = cGetFileList;
        poller.set_writable(server.get_fd(), true);
    }
    else if (cmd == "GET") {
        cin >> arg;
        get_req = "G";
        get_req.push_back(arg.length());
        get_req += arg;
        s_tmp = get_req;
        c_state = cSending;
        c_nstate = cGetFileServerSide;
        filename = dir_path + arg;
        poller.set_writable(server.get_fd(), true);
    }
    else if (cmd == "QUIT") {
        c_state = cClosing;
    }
}

void send_server() {
    static unsigned int pos = 0;
    int i = server.send(s_tmp, pos);
    if (i == -1) {
        c_state = cClosing;
        return;
    }
    pos += i;
    if (pos == s_tmp.length()) {
        s_tmp.clear();
        pos = 0;
        c_state = c_nstate;
        poller.set_writable(server.get_fd(), false);
        if (c_state == cSentFileList) {
            cout << "list of local files was sent to the server" << endl;
            c_state = cWaitingForUser;
        }
    }
}

void welcome() {
    unsigned char len;
    char msg[256] = {0};
    int r = server.recv(s_tmp);
    if (r == -1) {
        c_state = cClosing;
        return;
    }
    istringstream s(s_tmp);
    s.read(reinterpret_cast<char *>(&len), 1);
    s.get(msg, len + 1);
    if (s.gcount() != len) {
        return;
    }
    s_tmp.clear();
    cout << msg << endl;
    c_state = cSendFileList;
    poller.set_writable(server.get_fd(), true);
}

void get_file_list() {
    string s;
    unsigned char len;
    char name[256] = {0};
    if (!s_tmp.empty()) {
        s = s_tmp;
        s_tmp.clear();
    }
    int r = server.recv(s);
    istringstream st(s);
    if (r == -1) {
        c_state = cClosing;
        return;
    }
    while (st.read(reinterpret_cast<char *>(&len), 1)) {
        if (len == 0) {
            s_tmp.clear();
            c_state = cWaitingForUser;
            break;
        }
        st.get(name, len + 1);
        if (st.gcount() != len) {
            s_tmp.push_back(len);
            s_tmp.append(name, st.gcount());
            break;
        }
        cout << name << endl;
    }
}

void send_file_list() {
    DIR *dir;
    if ((dir = opendir(dir_path.c_str())) == NULL) {
        //bail out
        c_state = cClosing;
        closedir(dir);
        return;
    }
    struct dirent *ent;
    while ((ent = readdir(dir)) != NULL) {
        string name = ent->d_name;
        if (name[0] == '.')
            continue;
        s_tmp.push_back(name.length());
        s_tmp += name;
    }
    closedir(dir);
    s_tmp.push_back(0);
    c_state = cSending;
    c_nstate = cSentFileList;
    poller.set_writable(server.get_fd(), true);
}

void get_file() {
    static unsigned int file_size = 0;
    static unsigned int pos = 0;
    int r = file_sender->recv(file_sender->tmp);
    istringstream s(file_sender->tmp);
    file_sender->tmp.clear();
    ofstream f(filename, ofstream::out | ofstream::app);
    if (r == -1) {
        goto error;
    }
    if (file_size == 0) {
        s.read(reinterpret_cast<char *>(&file_size), 4);
        if (s.gcount() != sizeof file_size) {
            file_size = 0;
            file_sender->tmp = s.str();
            return;
        }
        file_size = ntohl(file_size);
    }
    pos += r;
    if (!f) {
        goto error;
    }
    f << s.rdbuf();
    if (!f) {
        goto error;
    }
    if (pos >= file_size) {
        //read the whole file
        //signal server success
        s_tmp = "Y";
        c_state = cSending;
        c_nstate = cWaitingForUser;
        poller.set_writable(server.get_fd(), true);
        file_size = pos = 0;
        poller.remove_fd(file_sender->get_sock_fd());
        file_sender.reset();
        cout << "transfer complete" << endl;
    }
    return;
error:
    cout << "transfer failed" << endl;
    file_size = pos = 0;
    s_tmp = "N";
    c_state = cSending;
    c_nstate = cWaitingForUser;
    poller.set_writable(server.get_fd(), true);
    poller.remove_fd(file_sender->get_sock_fd());
    file_sender.reset();
}

void handle_sender() {
    if (file_sender->get_state() == sSendall) {
        //we are sending the request
        file_sender->sendall();
        if (file_sender->get_state() != sSendall)
            poller.set_writable(file_sender->get_sock_fd(), false);
    }
    else if (file_sender->get_state() == sSendingFile) {
        get_file();
    }
    else {
        //close connection.
        poller.remove_fd(file_sender->get_sock_fd());
        file_sender.reset();
    }
}

void get_file_server_side() {
    unsigned ip;
    poller.set_writable(server.get_fd(), false);
    int r = server.recv(s_tmp);
    if (r == -1) {
        c_state = cClosing;
        return;
    }
    istringstream s(s_tmp);
    s.read(reinterpret_cast<char *>(&ip), 4);
    if (s.gcount() != 4)
        return;
    if (ip == 0) {
        cerr << "File unavailable." << endl;
        filename.clear();
        c_state = cWaitingForUser;
        return;
    }
    Socket sock;
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof addr);
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = ip;
    addr.sin_port = htons(CLIENT_PORT);
    sock.connect(addr);
    sock.set_non_blocking();
    file_sender = make_shared<Connection>(sock);
    poller.add_fd(sock.get_fd());
    file_sender->tmp = get_req;
    get_req.clear();
    file_sender->state_transition(sSendall, sSendingFile);
    poller.set_writable(sock.get_fd(), true);
}

void handle_server() {
        switch (c_state) {
            case cWelcome:
                welcome();
                break;
            case cSendFileList:
                send_file_list();
                break;
            case cSentFileList:
                break;
            case cGetFileList:
                get_file_list();
                break;
            case cGetFileServerSide:
                get_file_server_side();
                break;
            case cSending:
                send_server();
                break;
            case cWaitingForUser:
                poller.set_writable(server.get_fd(), false);
                break;
            case cClosing:
                return;
            default:
                c_state = cClosing;
                break;
        }
}

void remove_connection(ConnectionPtr c) {
    int fd = c->get_sock_fd();
    poller.remove_fd(fd);
    connections.erase(fd);
}

void send_file(ConnectionPtr c) {
    //if filename is empty, we need to parse the request.
    if (c->filename.empty()) {
        int r = c->recv(c->tmp);
        if (r == -1) {
            //he closed connection already?
            remove_connection(c);
            return;
        }
        if (r == 0 && c->tmp.empty()) {
            return;
        }
        istringstream s(c->tmp);
        char req;
        req = s.get();
        if (req != 'G') {
            remove_connection(c);
            return;
        }
        unsigned char len;
        char filename[256] = {0};
        s.read(reinterpret_cast<char *>(&len), 1);
        s.get(filename, len + 1);
        if (s.gcount() != len) {
            return;
        }
        c->tmp.clear();
        c->filename = dir_path + filename;
        c->total_pos = 0;
        poller.set_writable(c->get_sock_fd(), true);
        return;
    }
    ifstream f(c->filename, ifstream::in | ifstream::binary);
    if (!f) {
        cerr << "failed to open file" << endl;
        remove_connection(c);
        return;
    }
    char buffer[BUFFERSIZE + 1] = {0};
    if (c->total_pos == 0) {
        //we haven't sent the file.
        //send size
        f.seekg(0, ifstream::end);
        unsigned int size_in_net_order = htonl(f.tellg());
        c->tmp.append((char *)&size_in_net_order, sizeof size_in_net_order);
    }
    f.seekg(c->total_pos);
    f.read(buffer, BUFFERSIZE);
    if (f.bad()) {
        remove_connection(c);
        return;
    }
    else {
        c->tmp.append(buffer, f.gcount());
        c->total_pos += f.gcount();
        if (f.eof()) {
            //now we are waiting for client to disconnect.
            c->state_transition(sSendall, sClosing);
            poller.set_writable(c->get_sock_fd(), true);
        }
        else {
            c->state_transition(sSendall, sSendingFile);
            poller.set_writable(c->get_sock_fd(), true);
        }
    }
}

void handle_connection(ConnectionPtr c) {
    try {
        switch (c->get_state()) {
            case sSendingFile:
                //sending a new piece
                send_file(c);
                break;
            case sSendall:
                //sending the loaded buffer
                c->sendall();
                if (c->get_state() == sClosing) {
                    remove_connection(c);
                }
                break;
            case sClosed:
            default:
                remove_connection(c);
                break;
        }
    }
    catch (exception &ex) {
        cerr << ex.what() << endl;
        remove_connection(c);
    }
    if (c->get_state() == sClosed) {
        remove_connection(c);
    }
}

void accept_connections() {
    try {
        while (true) {
            Socket s = listening.accept();
            if (s.get_fd() == -1) {
                //no more connections
                return;
            }
            s.set_non_blocking();
            ConnectionPtr c = std::make_shared<Connection>(s);
            c->state_transition(sSendingFile, sSendingFile);
            connections[s.get_fd()] = c;
            poller.add_fd(s.get_fd());
        }
    }
    catch (SocketException &ex) {
        cerr << ex.what() << endl;;
        return;
    }
}

void loop() {
    vector<int> ready;
    while (c_state != cClosing) {
        ready = poller.poll();
        if (ready.empty()) {
            //select failed.
            return;
        }
        for (vector<int>::iterator it = ready.begin(), end = ready.end();
                it != end;
                it++) {
            if (*it == 0) {
                if (c_state == cWaitingForUser)
                    //handle user input
                    handle_user_input();
            }
            else if (*it == server.get_fd()) {
                handle_server();
            }
            else if (file_sender && *it == file_sender->get_sock_fd()) {
                handle_sender();
            }
            else if(*it == listening.get_fd()) {
                //new connection
                accept_connections();
            }
            else {
                ConnectionPtr c = connections[*it];
                handle_connection(c);
            }
        }
    }
}

int main(int argc, char **argv) {
    vector<string> args(argv, argv + argc);
    if (args.size() == 1 || args.size() > 4) {
        cerr << "Wrong invocation. Invoke as file_client dir [hostname [port]]" << endl;
        return 1;
    }
    if (args.size() == 2) {
        args.push_back("localhost");
    }
    if (args.size() == 3) {
        args.push_back(SERVER_PORT);
    }
    dir_path = args[1];
    if (*dir_path.end() != '/') {
        dir_path.push_back('/');
    }
    try {
        poller.add_fd(0); //STDIN
        server.connect(args[2], args[3]);
        server.set_non_blocking();
        poller.add_fd(server.get_fd());
        listening.create();
        listening.bind(CLIENT_PORT);
        listening.listen();
        listening.set_non_blocking();
        poller.add_fd(listening.get_fd());
        c_state = cWelcome;
        loop();
    }
    catch (exception &e) {
        cerr << e.what() << endl;
    }
    server.close();
    listening.close();
}
