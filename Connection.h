#ifndef CONNECTION_H
#define CONNECTION_H

#include "Socket.h"
#include <memory>

enum ConnectionState {
    sWaitingForCmd,
    sSendFilelist,
    sRecvFilelist,
    sSendall,
    sSendingFile,
    sClosing,
    sClosed
};

class Connection {
    public:
        Connection(Socket s);
        virtual ~Connection();
        int send(const std::string &s) const;
        int recv(std::string &s) const;
        int get_sock_fd() const;
        unsigned get_id() const;
        std::string get_address() const;
        void sendall();
        enum ConnectionState get_state() const;
        void state_transition(enum ConnectionState state, enum ConnectionState next_state);
        unsigned int pos;
        unsigned int total_pos;
        Socket socket;
        std::string tmp;
        std::string filename;
    private:
        static unsigned counter;
        unsigned id;
        enum ConnectionState state;
        enum ConnectionState next_state;
};
typedef std::shared_ptr<Connection> ConnectionPtr;
#endif
