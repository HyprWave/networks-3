#include "Poller.h"
#include <iostream>

Poller::Poller() :max_fd(-2) {
    FD_ZERO(&read_fds);
    FD_ZERO(&write_fds);
}

void Poller::add_fd(int fd) {
    FD_SET(fd, &read_fds);
    if (fd > max_fd)
        max_fd = fd;
}

void Poller::remove_fd(int fd) {
    FD_CLR(fd, &read_fds);
    FD_CLR(fd, &write_fds);
    if (fd == max_fd) {
        while(!FD_ISSET(max_fd, &read_fds) && max_fd >= 0)
            max_fd--;
    }
}

void Poller::set_writable(int fd, bool write) {
    if (write)
        FD_SET(fd, &write_fds);
    else
        FD_CLR(fd, &write_fds);
}

vector<int> Poller::poll() const {
    vector<int> ret;
    fd_set read_fds_ = read_fds;
    fd_set write_fds_ = write_fds;

    int events = select(max_fd + 1, &read_fds_, &write_fds_, 0, 0);
    if (events == -1) {
        //report error?
    }
    for (int i = 0; i <= max_fd && events > 0; i++) {
        if (FD_ISSET(i, &read_fds_) || FD_ISSET(i, &write_fds_)) {
            ret.push_back(i);
            if (i != 0)
            events--;
        }
    }
    return ret;
}
