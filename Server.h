#ifndef SERVER_H
#define SERVER_H

#define WELCOME_MSG "Welcome to the server! yay ^_^"
#define WELCOME_LEN 30

#include <list>
#include <map>
#include "Socket.h"
#include "Connection.h"
#include "Poller.h"

using namespace std;

class Server {
    public:
        Server(unsigned short port);
        ~Server();
        void run();
    private:
        void accept_connections();
        void handle_client(ConnectionPtr c);
        void handle_request(ConnectionPtr c);
        void send_file(ConnectionPtr c);
        void send_filelist(ConnectionPtr c);
        void receive_filelist(ConnectionPtr c);
        void remove_client(ConnectionPtr c);
        Socket sock;
        Poller poller;
        map<int, ConnectionPtr> clients;
        multimap<string, ConnectionPtr> files;
};

#endif
