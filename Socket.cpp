#include "Socket.h"
#include <cstring>
#include <sstream>
#include <ios>

Socket::Socket(int fd):sockfd(fd)
{
    memset(&addr, 0, sizeof(addr));
}

void Socket::close()
{
    if (sockfd != -1) {
        ::close(sockfd);
        sockfd = -1;
    }
}

int Socket::get_fd() const {
    return sockfd;
}

void Socket::create() {
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
        throw SocketException("Failed to create socket.");
}

void Socket::bind(const unsigned short port) {
    if (sockfd == -1)
        throw SocketException("Invalid socket");
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(port);

    if (::bind(sockfd, (struct sockaddr *) &addr, sizeof addr) == -1)
        throw SocketException("Failed to bind.");
}

void Socket::listen() const {
    if (sockfd == -1)
        throw SocketException("Invalid socket");
    if (::listen(sockfd, 10) == -1)
        throw SocketException("Failed to listen.");
}

Socket Socket::accept() const {
    Socket s;
    socklen_t size = sizeof s.addr;
    int fd = ::accept(sockfd, (struct sockaddr *)&(s.addr), &size);
    if (fd == -1) {
        if (errno != EWOULDBLOCK || errno != EAGAIN) {
            throw SocketException("Failed to accept client.");
        }
    }
    s.sockfd = fd;
    return s;
}

void Socket::connect(const struct sockaddr_in &addr_i) {
    if (sockfd == -1)
        create();
    memcpy(&addr, &addr_i, sizeof addr_i);
    char b[INET_ADDRSTRLEN] = {0};
    ::inet_ntop(AF_INET, &addr.sin_addr, b, sizeof b);
    if (::connect(sockfd, (struct sockaddr *) &addr, sizeof addr) != 0)
        throw SocketException("Failed to connect.");
}

void Socket::connect(const std::string &hostname, const std::string &port) {
    struct addrinfo hints, *servinfo, *p;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    if (::getaddrinfo(hostname.c_str(), port.c_str(), &hints, &servinfo) != 0) {
        throw SocketException("Failed to connect: getaddrinfo failed");
    }
    for (p = servinfo; p != NULL; p = p->ai_next) {
        try {
            connect(*((struct sockaddr_in *)(p->ai_addr)));
            break;
        }
        catch (SocketException &e) {
            close();
        }
    }
    ::freeaddrinfo(servinfo);
    if (p == NULL) {
        throw SocketException("Failed to connect to " + hostname);
    }
}

int Socket::send(const std::string &s, int pos) const {
    const char *buffer = s.c_str() + pos;
    int len = s.length() - pos;
    int sent = 0;
    int n;
    do {
        n = ::send(sockfd, buffer + sent, len - sent, MSG_NOSIGNAL);
        if (n < 0) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                break;
            }
            throw SocketException("Failed to send.");
        }
        if (n == 0 && sent == 0) {
            return -1;
        }
        sent += n;
    } while (sent < len && n > 0);
    return sent;
}

int Socket::recv(std::string &s) const {
    char buffer[MAX_RECV + 1];
    memset(buffer, 0, MAX_RECV + 1);
    int read = 0;
    int rc;
    do {
        rc = ::recv(sockfd, buffer, MAX_RECV, 0);
        if (rc < 0) {
            if (rc == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
                break;
            }
            throw SocketException("Failed to recv.");
        }
        if (rc == 0 && read == 0)
            return -1;
        s.append(buffer, rc);
        read += rc;
    } while (read < MAX_RECV && rc > 0);
    return read;
}

std::string Socket::get_address() const {
    std::ostringstream s;
    s.write((char *)&addr.sin_addr.s_addr, 4);
    return s.str();
}

void Socket::set_non_blocking() {
    int opts = fcntl(sockfd, F_GETFL);
    if (opts < 0)
        throw SocketException("Failed to set non block");
    opts |= O_NONBLOCK;
    if (fcntl(sockfd, F_SETFL, opts) != 0)
        throw SocketException("Failed to set non block");
}

