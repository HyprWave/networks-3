#include "Connection.h"
#include <iostream>

unsigned Connection::counter = 0;

Connection::Connection(Socket s): socket(s), id(counter++) {}

Connection::~Connection() {
    socket.close();
}

int Connection::get_sock_fd() const {
    return socket.get_fd();
}

unsigned Connection::get_id() const {
    return id;
}

int Connection::send(const std::string &s) const {
    return socket.send(s);
}

int Connection::recv(std::string &s) const {
    return socket.recv(s);
}

std::string Connection::get_address() const {
    return socket.get_address();
}

enum ConnectionState Connection::get_state() const {
    return state;
}

void Connection::state_transition(enum ConnectionState state, enum ConnectionState next_state) {
    this->state = state;
    this->next_state = next_state;
}

void Connection::sendall() {
    int i = socket.send(tmp, pos);
    if (i == -1) {
        state = sClosed;
        return;
    }
    pos += i;
    if (pos == tmp.length()) {
        //sent all
        tmp.clear();
        pos = 0;
        state = next_state;
    }
}
