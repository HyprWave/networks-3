#ifndef SOCKET_H
#define SOCKET_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <arpa/inet.h>
#include <cerrno>
#include <exception>
#include <memory>

#define MAX_RECV 512

class SocketException : public std::exception {
    public:
        SocketException(const std::string &msg): msg(msg) {}
        const char *what() const throw() { return msg.c_str(); }
        ~SocketException() throw() {}
    private:
        std::string msg;
};


class Socket
{
    public:
        Socket(int fd=-1);
        void create();
        void close();
        void set_non_blocking();
        void bind(const unsigned short port);
        void listen() const;
        int get_fd() const;
        std::string get_address() const;
        Socket accept() const;
        void connect(const struct sockaddr_in &addr_i);
        void connect(const std::string &hostname, const std::string &port);
        int send(const std::string &s, int pos=0) const;
        int recv(std::string &s) const;
    private:
        static unsigned counter;
        int sockfd;
        struct sockaddr_in addr;
};

typedef std::shared_ptr<Socket> SocketPtr;

#endif // SOCKET_H
