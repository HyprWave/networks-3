#include "Server.h"
#include <iostream>
#include <cstdlib>

int main(int argc, char **argv) {
    unsigned short port = 2511;
    if (argc == 2) {
        port = atoi(argv[1]);
    }
    try {
        Server s(port);
        s.run();
    }
    catch (exception &ex) {
        std::cout << ex.what() << std::endl;
    }
}
