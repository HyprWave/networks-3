#include "Server.h"
#include <sstream>
#include <iostream>

Server::Server(unsigned short port) {
    sock.create();
    sock.bind(port);
    sock.listen();
    sock.set_non_blocking();
    poller.add_fd(sock.get_fd());
}

Server::~Server() {
    sock.close();
}

void Server::run() {
    vector<int> ready;
    while (true) {
        ready = poller.poll();
        if (ready.empty()) {
            //means select failed.
            return;
        }
        for (vector<int>::iterator it = ready.begin(), end = ready.end();
                it != end;
                it++) {
            if (*it == sock.get_fd()) {
                try {
                    accept_connections();
                }
                catch (SocketException &ex) {
                    return;
                }
            }
            else {
                ConnectionPtr c = clients[*it];
                handle_client(c);
            }
        }
    }
}

void Server::accept_connections() {
    while (true) {
        Socket s = sock.accept();
        if (s.get_fd() == -1) {
            //no more connections
            return;
        }
        s.set_non_blocking();
        ConnectionPtr c = std::make_shared<Connection>(s);
        c->tmp.push_back(WELCOME_LEN);
        c->tmp += WELCOME_MSG;
        c->pos = 0;
        c->state_transition(sSendall, sRecvFilelist);
        clients[s.get_fd()] = c;
        poller.add_fd(s.get_fd());
        poller.set_writable(s.get_fd(), true);
    }
}

void Server::handle_client(ConnectionPtr c) {
    try {
        switch (c->get_state()) {
            case sWaitingForCmd:
                handle_request(c);
                break;
            case sRecvFilelist:
                receive_filelist(c);
                break;
            case sSendall:
                c->sendall();
                if(c->get_state() != sSendall)
                    poller.set_writable(c->get_sock_fd(), false);
                break;
            case sSendingFile:
                send_file(c);
                break;
            default:
                break;
        }
    }
    catch (SocketException &ex) {
        c->state_transition(sClosed, sClosed);
    }
    if (c->get_state() == sClosed) {
        remove_client(c);
    }
}

void Server::handle_request(ConnectionPtr c) {
    string &s = c->tmp;
    int r = c->recv(s);
    if (r == -1) {
        c->state_transition(sClosed, sClosed);
        return;
    }
    if (r == 0 && s.empty()) {
        return;
    }
    istringstream st(s);
    char req;
    st >> req;
    if (req == 'D') {
        send_filelist(c);
    }
    else if (req == 'G') {
        //handle get request
        unsigned char len;
        char filename[256] = {0};
        st.read(reinterpret_cast<char *>(&len), 1);
        st.get(filename, len + 1);
        if (st.gcount() != len) {
            //c->tmp will contain the already read data, so it will be here next time.
            return;
        }
        c->tmp.clear();
        c->filename = filename;
        send_file(c);
    }
    else {
        //error, throw him out
        c->state_transition(sClosed, sClosed);
    }
}

void Server::remove_client(ConnectionPtr c) {
    //remove from file map
    multimap<string, ConnectionPtr>::iterator it = files.begin();
    while (it != files.end()) {
        if (it->second == c) {
            files.erase(it++);
        }
        else
            it++;
    }
    int fd = c->get_sock_fd();
    poller.remove_fd(fd);
    clients.erase(fd);
}

void Server::send_file(ConnectionPtr c) {
    string &filename = c->filename;
    if (c->get_state() == sSendingFile) {
        //handling response
        if (c->recv(c->tmp) == -1) {
            c->state_transition(sClosed, sClosed);
            return;
        }
        if (c->tmp[0] == 'Y') {
            files.insert(pair<string, ConnectionPtr>(c->filename, c));
        }
        else if(c->tmp[0] != 'N') {
            //not following protocol
            c->state_transition(sClosed, sClosed);
            return;
        }
        c->filename.clear();
        c->tmp.erase(c->tmp.begin());
        c->state_transition(sWaitingForCmd, sWaitingForCmd);
        return;
    }
    multimap<string, ConnectionPtr>::iterator it = files.find(filename);
    if (it == files.end() || it->second->get_id() == c->get_id()) {
        c->tmp.clear();
        c->tmp.append(4, 0);
        c->filename.clear();
        c->state_transition(sSendall, sWaitingForCmd);
    }
    else {
        c->tmp = it->second->get_address();
        c->state_transition(sSendall, sSendingFile);
    }
    poller.set_writable(c->get_sock_fd(), true);

}

void Server::send_filelist(ConnectionPtr c) {
    string &s = c->tmp;
    s.clear();
    //preparing filelist
    for (multimap<string, ConnectionPtr>::iterator it = files.begin(), end = files.end();
            it != end;
            it = files.upper_bound(it->first)) {
        s.push_back(it->first.length());
        s += it->first;
    }
    s.push_back(0); // end of list
    poller.set_writable(c->get_sock_fd(), true);
    c->state_transition(sSendall, sWaitingForCmd);
}

void Server::receive_filelist(ConnectionPtr c) {
    string s;
    unsigned char len;
    char name[256] = {0};
    if (!c->tmp.empty()) {
        s = c->tmp;
        c->tmp.clear();
    }
    int r = c->recv(s);
    istringstream st(s);
    if (r == -1) {
        c->state_transition(sClosed, sClosed);
        return;
    }
    while (st.read(reinterpret_cast<char *>(&len), 1)) {
        if (len == 0) {
            c->state_transition(sWaitingForCmd, sWaitingForCmd);
            if (st.tellg() < (unsigned)s.length())
                c->tmp = s.substr(st.tellg());
            break;
        }
        st.get(name, len + 1);
        if (st.gcount() != len) {
            c->tmp.push_back(len);
            c->tmp.append(name, st.gcount());
            break;
        }
        files.insert(pair<string, ConnectionPtr>(name, c));
    }
}
